import React from "react";
import {Li} from './li/li';
import Af from '../af/af';
import InputSearch from "../input-search/input-search";
import InputRadio from "../input-radio/input-radio";


import './menu.css'


import search from './img-icons/search.svg'
import dashboard from './img-icons/dashboard.svg'
import revenue from './img-icons/revenue.svg'
import notific from './img-icons/notific.svg'
import analytics from './img-icons/analytics.svg'
import inventory from './img-icons/inventory.svg'
import logout from './img-icons/logout.svg'
import lightmode from './img-icons/lightmode.svg'

function Menu (){
    return (
        <>  
       { 
     <div className="menu"> 
     <ul >
    <li><Af></Af></li>
    <Li icon={search} text={'Search'} id="searchInp"/>
    <Li icon={dashboard}  text ='Dashboard'/>
    <Li icon={revenue}  text ='Revenue'/>
    <Li icon={notific}  text ='Notifications'/>
    <Li icon={analytics}  text ='Analytics'/>
    <Li icon={inventory}  text ='Inventory'/>
   
    <div className='bottom'>

    <Li icon={logout}  text ='Logout'/>

    <div id='mode'>
      <Li  icon={lightmode}  text ='Lightmode'> </Li>
      <div id="inputs">
      <InputRadio />
     
      </div>
      
    </div>

    </div>
   
    </ul>
    </div> 
        }
        </>
    )

}
export default Menu